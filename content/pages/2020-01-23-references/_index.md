---
title: References
---

## Technology

- How to Build a Low-tech Website? [(link)](https://solar.lowtechmagazine.com/2018/09/how-to-build-a-lowtech-website.html)
- Jenny Odell: "How to Do Nothing: Resisting the Attention Economy" [(link)](https://www.youtube.com/watch?v=izjlP9qtmBU)
- My website is a shifting house next to a river of knowledge [(link)](https://thecreativeindependent.com/people/laurel-schwulst-my-website-is-a-shifting-house-next-to-a-river-of-knowledge-what-could-yours-be/)
- Death to Bullshit [(link)](http://deathtobullshit.com/)
- The Critical Engineering Manifesto [(link)](https://criticalengineering.org/en)
- This is your phone on feminism [(link)](https://conversationalist.org/2019/09/13/feminism-explains-our-toxic-relationships-with-our-smartphones/)
- Calm Technology [(link)](https://calmtech.com/)


## Plants

- Thijs Biersteker & Stefano Mancuso - Nous les Arbres [(link)](https://www.youtube.com/watch?v=ukcOmdeVQcM)
- Voice of Nature / Thijs Biersteker [(link)](https://www.youtube.com/watch?v=AGbmut3hy7w)
- “Plant Lamps” Turn Dirt and Vegetation into a Power Source [(link)](https://www.technologyreview.com/s/543781/plant-lamps-turn-dirt-and-vegetation-into-a-power-source/)
- Research progress on electrical signals in higher plants [(link)](https://www.sciencedirect.com/science/article/pii/S1002007109000161)
- Meet the Sonic Artist Making Music with Plants: Sound Builders [(link)](https://www.youtube.com/watch?v=wYU18eiiFt4)
- Bully A Plant: Say No To Bullying [(link)](https://www.youtube.com/watch?v=Yx6UgfQreYY&feature=youtu.be)
- Singing plant [(link)](https://www.instructables.com/id/Singing-plant-Make-your-plant-sing-with-Arduino-/)
- Music of the plants [(link)](https://upliftconnect.com/music-of-the-plants/)
- Plant talk [(link)](https://www.the-scientist.com/features/plant-talk-38209)

## Communication

- Books: Empire of the Ants (Les Fourmis) [(link)](https://en.wikipedia.org/wiki/Empire_of_the_Ants_(novel))

## Art installation

- One Hundred and Eight - Power of Making, Nils Völker [(link)](https://vimeo.com/27197947)
- Behaviours of Light [(link)](https://vimeo.com/85159748)

## Robotic

- Experiments in Biomimetic Technology Applied to Architecture Components [(link)](https://www.youtube.com/watch?v=MhhFja6tghc)
- Robotic architecture [(link)](https://www.youtube.com/watch?v=m5VWHUvKt48)
- Soft Roboto [(link)](https://www.youtube.com/watch?v=q2Q-taHAo7Q)
- Soft Robots Inspired by Origami [(link)](https://www.youtube.com/watch?v=ARSKahntQDA)

## Fab Academy References (related to plants)

- The hostile plant [(link)](http://fab.cba.mit.edu/classes/863.18/EECS/people/miana/finals.html)
- hello.plant [(link)](http://fab.cba.mit.edu/classes/863.09/people/lifeform/07/index.html)
- Walking plants [(link)](http://fab.cba.mit.edu/classes/863.10/people/hiro.tanaka/pages/page_01.html)
- Breathing Flowerpot [(link)](http://fab.cba.mit.edu/classes/863.19/Harvard/people/hsingh/project.html)
- Automated indoor hanging garden [(link)](http://fab.academany.org/2019/labs/berytech/students/nagi-abdelnour/final%20project%20dev.html)
- Replicating superhydrophobic plant leaves [(link)](http://archive.fabacademy.org/archives/2017/woma/students/238/assignment12.html)


